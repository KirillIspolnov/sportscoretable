package org.kllbff.sportscoreboard;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.TextView;

public class TeamShower {
    private ImageView firstIcon, secondIcon;
    private TextView firstName, secondName;

    public TeamShower(ImageView firstIcon, TextView firstName, ImageView secondIcon, TextView secondName) {
        this.firstIcon = firstIcon;
        this.firstName = firstName;
        this.secondIcon = secondIcon;
        this.secondName = secondName;
    }

    public void showFirst(Drawable icon, String name) {
        firstIcon.setImageDrawable(icon);
        firstName.setText(name);
    }

    public void showSecond(Drawable icon, String name) {
        secondIcon.setImageDrawable(icon);
        secondName.setText(name);
    }
}
