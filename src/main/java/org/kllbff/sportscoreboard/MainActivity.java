package org.kllbff.sportscoreboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {
    public static final String KEY_FIRST_SCORE = "team-score-first";
    public static final String KEY_SECOND_SCORE = "team-score-second";

    private Timer timer;
    private TeamShower teamShower;
    private TextView firstTeamScoreView;
    private TextView secondTeamScoreView;
    private int firstScore, secondScore;
    private int last;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timer = new Timer(new Handler(), findViewById(R.id.timer),
                          findViewById(R.id.timer_info), findViewById(R.id.timer_extra), Game.FOOTBALL);

        firstTeamScoreView = findViewById(R.id.team_first_scores);
        secondTeamScoreView = findViewById(R.id.team_second_scores);

        if(savedInstanceState != null) {
            timer.restore(savedInstanceState);
            if(timer.isPaused()) {
                timer.resume();
                findViewById(R.id.tools_pause).setVisibility(View.GONE);
                findViewById(R.id.tools_start).setVisibility(View.VISIBLE);
            }

            firstScore = savedInstanceState.getInt(KEY_FIRST_SCORE, 0);
            secondScore = savedInstanceState.getInt(KEY_SECOND_SCORE, 0);
            updateScoreViews();
        }

        teamShower = new TeamShower(findViewById(R.id.team_first_emblem), findViewById(R.id.team_first_name),
                                    findViewById(R.id.team_second_emblem), findViewById(R.id.team_second_name));

        Resources resources = getResources();
        teamShower.showFirst(ResourcesCompat.getDrawable(resources, R.drawable.forest_friends, getTheme()),
                             getString(R.string.team_forest_friends));
        teamShower.showSecond(ResourcesCompat.getDrawable(resources, R.drawable.underwater_undead, getTheme()),
                              getString(R.string.team_underwater_undead));
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        timer.save(bundle);
        bundle.putInt(KEY_FIRST_SCORE, firstScore);
        bundle.putInt(KEY_SECOND_SCORE, secondScore);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void updateScoreViews() {
        firstTeamScoreView.setText(String.valueOf(firstScore));
        secondTeamScoreView.setText(String.valueOf(secondScore));
    }

    public void onFirstTeamClick(View view) {
        if(timer.isPaused()) {
            Snackbar.make(firstTeamScoreView, R.string.game_paused, Snackbar.LENGTH_SHORT).show();
            return;
        }

        firstScore++;
        last = 1;
        updateScoreViews();
    }

    public void onSecondTeamClick(View view) {
        if(timer.isPaused()) {
            Snackbar.make(secondTeamScoreView, R.string.game_paused, Snackbar.LENGTH_SHORT).show();
            return;
        }

        secondScore++;
        last = 2;
        updateScoreViews();
    }

    public void onStartClick(View view) {
        timer.resume();
        findViewById(R.id.tools_start).setVisibility(View.GONE);
        findViewById(R.id.tools_pause).setVisibility(View.VISIBLE);
    }

    public void onPauseClick(View view) {
        timer.pause();
        findViewById(R.id.tools_pause).setVisibility(View.GONE);
        findViewById(R.id.tools_start).setVisibility(View.VISIBLE);
    }

    public void onResetClick(View view) {
        timer.reset();

        firstScore = 0;
        secondScore = 0;
        updateScoreViews();
        findViewById(R.id.tools_pause).setVisibility(View.GONE);
        findViewById(R.id.tools_start).setVisibility(View.VISIBLE);
    }

    public void onCancelClick(View view) {
        switch(last) {
            case 1: firstScore--; break;
            case 2: secondScore--; break;
            default:
                Snackbar.make(secondTeamScoreView, R.string.game_cancel_cannot, Snackbar.LENGTH_SHORT).show();
            break;
        }
        last = 0;
        updateScoreViews();
    }
}
