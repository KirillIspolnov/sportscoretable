package org.kllbff.sportscoreboard;

public enum Game {
    BASKETBALL (4, 12, R.string.game_basketball_period),
    FOOTBALL   (2, 1, R.string.game_football_period),
    HOCKEY     (3, 20, R.string.game_hockey_period),
    RUGBY      (2, 40, R.string.game_rugby_period);

    private int periodsCount;
    private int periodsTime;
    private int periodNameResId;

    private Game(int periodsCount, int periodsTime, int periodNameResId) {
        this.periodsCount = periodsCount;
        this.periodsTime = periodsTime;
        this.periodNameResId = periodNameResId;
    }

    public int getPeriodsCount() {
        return periodsCount;
    }

    public int getPeriodsTime() {
        return periodsTime;
    }

    public int getPeriodNameResId() {
        return periodNameResId;
    }
}
