package org.kllbff.sportscoreboard;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

public class Timer {
    public static final String KEY_MINUTES = "timer-minutes";
    public static final String KEY_SECONDS = "timer-seconds";
    public static final String KEY_PERIOD = "timer-period";

    private boolean timerPaused, afterRestore;
    private Handler handler;
    private TextView timerView, infoView, extraTimeView;
    private Game game;
    private int minutes, seconds, period;

    public Timer(Handler handler, TextView timerView, TextView infoView, TextView extraTimeView, Game game) {
        this.handler = handler;
        this.timerView = timerView;
        this.infoView = infoView;
        this.extraTimeView = extraTimeView;
        this.game = game;

        this.minutes = 0;
        this.seconds = 0;
        this.period = 0;
        this.timerPaused = true;
        this.afterRestore = false;
    }

    public boolean isPaused() {
        return timerPaused;
    }

    public void reset() {
        this.timerPaused = true;
        minutes = 0;
        seconds = -1;
        period = 1;
        afterRestore = false;
        updateTimerView();
        updateExtraTimeView();
        period = 0;
    }

    public void resume() {
        if(!afterRestore) {
            period++;

            minutes = 0;
            seconds = -1;
            extraTimeView.setVisibility(View.GONE);

            if(updateTimerView()) {
                return;
            }
        } else {
            afterRestore = false;
        }

        timerPaused = false;
        postTask();
    }

    public void pause() {
        timerPaused = true;
    }

    public void save(Bundle bundle) {
        bundle.putInt(KEY_MINUTES, minutes);
        bundle.putInt(KEY_SECONDS, seconds);
        bundle.putInt(KEY_PERIOD, period);
    }

    public void restore(Bundle bundle) {
        minutes = bundle.getInt(KEY_MINUTES, 0);
        seconds = bundle.getInt(KEY_SECONDS, 0);
        period = bundle.getInt(KEY_PERIOD, 1);

        updateTimerView();
        updateExtraTimeView();
        afterRestore = true;
    }

    private void updateExtraTimeView() {
        if(minutes >= game.getPeriodsTime()) {
            extraTimeView.setVisibility(View.VISIBLE);
        } else {
            extraTimeView.setVisibility(View.GONE);
        }
    }

    /**
     * @return true if game end
     */
    private boolean updateTimerView() {
        if(period > game.getPeriodsCount()) {
            infoView.setText(R.string.game_end);
            return true;
        }

        infoView.setText(infoView.getContext().getString(game.getPeriodNameResId(), period));

        StringBuilder time = new StringBuilder();

        seconds++;
        if(seconds == 60) {
            seconds = 0;
            minutes++;
        }

        if(minutes < 10) {
            time.append(0);
        }
        time.append(minutes);
        time.append(":");

        if(seconds < 10) {
            time.append(0);
        }
        time.append(seconds);

        timerView.setText(time);
        return false;
    }

    private void postTask() {
        handler.postDelayed(new Task(), 1000L);
    }

    private class Task implements Runnable {
        public void run() {
            if(timerPaused) {
                return;
            }

            updateTimerView();
            updateExtraTimeView();

            Timer.this.postTask();
        }
    }
}
